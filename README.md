# ECU Wright Brothers Centennial Digital Exhibit

Jekyll site for the legacy Wright Brothers Centennial Digital Exhibit.
* [http://digital.lib.ecu.edu/exhibits/wright](http://digital.lib.ecu.edu/exhibits/wright)

## Modification

Modification requires building the site with [Jekyll](https://jekyllrb.com/), which requires Ruby.

1. Clone repo
2. $ cd wright
3. $ bundle install
4. $ bundle exec jekyll serve

This should make [http://localhost:4000/exhibits/wright/](http://localhost:4000/exhibits/wright/) available in your browser to view the site (trailing slash is important here). After changes are made, copy the contents of the _site directory to the live webserver.

